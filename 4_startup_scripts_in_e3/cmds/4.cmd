require stream
require iocstats

epicsEnvSet "TOP" "$(E3_CMD_TOP)/.."

system "bash $(TOP)/tools/random.bash"

iocshLoad "$(TOP)/tools/random.cmd"

epicsEnvSet("P", "IOC-$(NUM)")
epicsEnvSet("IOCNAME", "$(P)")
epicsEnvSet("PORT", "MOTOR")

epicsEnvSet("STREAM_PROTOCOL_PATH", ".:$(TOP)/db")

drvAsynIPPortConfigure("$(PORT)", "127.0.0.1:9999", 0, 0, 0)

dbLoadRecords("$(TOP)/db/example_motor.db", "P=$(IOCNAME),S=:,PORT=$(PORT)")
dbLoadRecords("iocAdminSoft.db","IOC=$(IOCNAME):IocStat")

iocInit()


