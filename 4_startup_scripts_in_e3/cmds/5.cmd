require stream
require iocstats
require recsync
require autosave

epicsEnvSet "TOP" "$(E3_CMD_TOP)/.."

system "bash $(TOP)/tools/random.bash"
iocshLoad "$(TOP)/tools/random.cmd"

epicsEnvSet("P", "IOC-$(NUM)")
epicsEnvSet("IOCNAME", "$(P)")
epicsEnvSet("PORT", "MOTOR")

epicsEnvSet("STREAM_PROTOCOL_PATH", ".:$(TOP)/db")

drvAsynIPPortConfigure("$(PORT)", "127.0.0.1:9999", 0, 0, 0)

dbLoadRecords("$(TOP)/db/example_motor.db", "P=$(IOCNAME),S=:,PORT=$(PORT)")

iocshLoad("$(iocstats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")
iocshLoad("$(recsync_DIR)/recsync.iocsh",  "IOCNAME=$(IOCNAME)")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "IOCNAME=$(IOCNAME), AS_TOP=$(TOP), IOCDIR=$(IOCNAME)")

iocInit

dbl > "$(TOP)/$(IOCNAME)_PVs.list"


