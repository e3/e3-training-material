Very Simple E3 Training Exercise
==

This directory contains all that you need to work with [Chapter 4](http://e3.pages.esss.lu.se/kb/training/workbook/4_startup_scripts.html) of the e3
training material. In particular, it includes a simulated device and a number of startup scripts to start an e3 IOC in order to communicated with
the simulated device.
## Simulator

This is the simple simulator which simulates the simple serial device through telnet. This uses an example device from
[Lewis](https://github.com/ess-dmsc/lewis.git), a general-purpose device simulator. Included are the necessary protocol
and database files to communicate with this device.
